package me.nothixal.randomserver;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import me.nothixal.randomserver.commands.RandomServerCommand;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

public final class RandomServer extends Plugin {

  private Configuration configFile;
  private Configuration messagesFile;

  @Override
  public void onEnable() {
    registerCommands();

    try {
      makeConfigs();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    loadConfigs();
  }

  @Override
  public void onDisable() {
    // Plugin shutdown logic
  }

  private void loadConfigs() {
    try {
      configFile = ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File(getDataFolder(), "config.yml"));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    try {
      messagesFile = ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File(getDataFolder(), "messages.yml"));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public void reload() {
    loadConfigs();
  }

  private void registerCommands() {
    ProxyServer.getInstance().getPluginManager().registerCommand(this, new RandomServerCommand("randomserver"));
  }

  public void makeConfigs() throws IOException {
    // Create plugin config folder if it doesn't exist
    if (!getDataFolder().exists()) {
      getLogger().info("Created config folder: " + getDataFolder().mkdir());
    }

    File configFile = new File(getDataFolder(), "config.yml");
    File messagesFile = new File(getDataFolder(), "messages.yml");

    // Copy default config if it doesn't exist
    if (!configFile.exists()) {
      FileOutputStream outputStream = new FileOutputStream(configFile); // Throws IOException
      InputStream in = getResourceAsStream("config.yml"); // This file must exist in the jar resources folder
      in.transferTo(outputStream); // Throws IOException
    }

    if (!messagesFile.exists()) {
      FileOutputStream outputStream = new FileOutputStream(messagesFile); // Throws IOException
      InputStream in = getResourceAsStream("messages.yml"); // This file must exist in the jar resources folder
      in.transferTo(outputStream); // Throws IOException
    }
  }

  public Configuration getConfig() {
    return configFile;
  }

  public Configuration getMessages() {
    return messagesFile;
  }
}
