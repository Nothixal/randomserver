package me.nothixal.randomserver.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import me.nothixal.randomserver.RandomServer;
import me.nothixal.randomserver.utils.StringUtil;
import me.nothixal.randomserver.utils.Utils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

public class RandomServerCommand extends Command implements TabExecutor {

  private final CountDownLatch latch = new CountDownLatch(1);
  private final ExecutorService executorService = Executors.newCachedThreadPool();
  private final RandomServer plugin;

  public RandomServerCommand(String name) {
    super(name);
    plugin = (RandomServer) ProxyServer.getInstance().getPluginManager().getPlugin("RandomServer");
  }

  @Override
  public void execute(CommandSender sender, String[] args) {
    if (!(sender instanceof ProxiedPlayer player)) {
      return;
    }

    int length = args.length;

    if (length == 0) {
      connectToRandomServer(player, new ArrayList<>(ProxyServer.getInstance().getServers().keySet()));
      return;
    }

    if (length == 1) {
      if (args[0].equalsIgnoreCase("reload")) {
        plugin.reload();

        BaseComponent[] component = new ComponentBuilder()
            .appendLegacy(Utils.colorize(plugin.getMessages().getString("reload-message")))
            .create();
        player.sendMessage(component);
        return;
      }

      List<String> shortenedServerList = new ArrayList<>();

      for (ServerInfo server : ProxyServer.getInstance().getServers().values()) {
        if (server.getName().startsWith(args[0])) {
          shortenedServerList.add(server.getName());
        }
      }

      connectToRandomServer(player, shortenedServerList);
    }
  }

  @Override
  public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
    if (args.length == 1) {
      List<String> commands = Arrays.asList("reload");
      List<String> completions = new ArrayList<>();

      StringUtil.copyPartialMatches(args[0], commands, completions);
      Collections.sort(completions);
      return completions;
    }

    if (args.length >=2) {
      return new ArrayList<>();
    }

    return null;
  }

  private void connectToRandomServer(ProxiedPlayer player, List<String> servers) {
    removeExclusions(player, servers);

    if (servers.isEmpty()) {
      BaseComponent[] component = new ComponentBuilder()
          .appendLegacy(Utils.colorize(plugin.getMessages().getString("no-servers-found")))
          .create();
      player.sendMessage(component);
      return;
    }

    player.connect(ProxyServer.getInstance().getServerInfo(servers.get(new Random().nextInt(servers.size()))));
  }

  private void removeExclusions(ProxiedPlayer player, List<String> servers) {
    /*
    * Remove the server the player is currently in.
    * */
    servers.remove(player.getServer().getInfo().getName());
//    serversToRemove.add(player.getServer().getInfo().getName());

    /*
    * Remove any excluded servers in the plugin config.
    * */
    for (String server : plugin.getConfig().getStringList("excluded-servers")) {
      servers.remove(server);
    }

    /*
    * Remove any server that is offline/unreachable.
    * */
    List<String> serversToRemove = new ArrayList<>();
    Runnable runnableA = () -> {
      for (String server : servers) {
        ProxyServer.getInstance().getServers().get(server).ping((result, error) -> {
          latch.countDown();

          if (error != null) {
            serversToRemove.add(server);
          }
        });
      }
    };

    Runnable runnableB = () -> {
      executorService.submit(runnableA);
      try {
        latch.await();
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
      }
    };

    executorService.submit(runnableB);

    try {
      Thread.sleep(10);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }

    for (String server : serversToRemove) {
      servers.remove(server);
    }
  }

}
