package me.nothixal.randomserver.utils;

import net.md_5.bungee.api.ChatColor;

public class Utils {

  public static String colorize(String msg) {
    return ChatColor.translateAlternateColorCodes('&', msg);
  }

}
